const assert = require('assert');

const apps = require('../../src/apps')

describe('#Apps', () => {
  
  describe('calculate', () => {

    it('Should - operator addition: ex. 3 + 5 = 8', async () => {
        const res = await apps.calculate('3 + 5');

        assert.equal(res, 8);
    });

    it('Should - operator subtraction: ex. 5 - 3 = 2', async () => {
        const res = await apps.calculate('5 - 3');

        assert.equal(res, 2);
    });

    it('Should - operator multiplication: ex. 5 * 3 = 15', async () => {
        const res = await apps.calculate('5 * 3');

        assert.equal(res, 15);
    });

    it('Should - operator division: ex. 6 / 3 = 2', async () => {
        const res = await apps.calculate('6 / 3');

        assert.equal(res, 2);
    });

  });

});
