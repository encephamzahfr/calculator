const assert = require('assert');
const { Given, Then } = require('cucumber');
const app = require('../../../src/apps');

Given('input {string}', (input) => {
    this.actualAnswer = app.calculate(input);
});

Then('return value {string}', (answer) => {
    assert.equal(this.actualAnswer, answer);
});