Feature: Simple Calculation

    Scenario Outline: Simple calculation
        Given input "<input>"
        Then return value "<answer>"

    Examples:
        | input     | answer              |
        | 3 + 3     | 6                   |
        | 3.4 + 3.4 | 6.8                 |
        | 3 - 3     | 0                   |
        | 3 * 3     | 9                   |
        | 3 / 3     | 1                   |
        | 3 3       | input not valid     |
        | 3 *       | input not valid     |