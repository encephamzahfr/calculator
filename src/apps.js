const operators = ['+', '-', '*', '/'];

const calculate = (input) => {
    input = input.split(' ');

    if(input.length < 3){
        return 'input not valid';
    }

    const firstValue = parseFloat(input[0]),
        operartor = input[1],
        secondValue = parseFloat(input[2]);

    const indexOfOperator = operators.findIndex( val => val === operartor);

    if(indexOfOperator === -1){
        return 'input not valid';
    }

    const opts = [
        add(firstValue, secondValue),
        subtract(firstValue, secondValue),
        multiplsecondValue(firstValue, secondValue),
        divide(firstValue, secondValue),
    ];

    return opts[indexOfOperator];
}

const add = (firstValue, secondValue) => {
    return firstValue + secondValue;
}

const subtract = (firstValue, secondValue) => {
    return firstValue - secondValue;
}

const multiplsecondValue = (firstValue, secondValue) => {
    return firstValue * secondValue;
}

const divide = (firstValue, secondValue) => {
    return firstValue / secondValue;
}

module.exports = {
    calculate
}